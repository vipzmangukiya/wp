<?php
function fn_arc_admin_custome_css() {
        wp_register_style( 'arc_admin_custome_css', get_template_directory_uri() . '/include/admin/css/arc_admin_custome.css', false, '1.0.0' );
        wp_enqueue_style( 'arc_admin_custome_css' );
}
add_action( 'admin_enqueue_scripts', 'fn_arc_admin_custome_css' );

function fn_arc_admin_custome_js() {
        wp_enqueue_script( 'arc_admin_custome_js', get_template_directory_uri().'/include/admin/js/arc_admin_custome.js' );
 		wp_enqueue_script('ajax-script');
        wp_localize_script( 'ajax-script', 'ARC',
            array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );

}
add_action( 'admin_enqueue_scripts', 'fn_arc_admin_custome_js' );
function wptuts_options_enqueue_scripts() {
        wp_enqueue_script('jquery');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
        wp_enqueue_script('media-upload');
}
add_action('admin_enqueue_scripts', 'wptuts_options_enqueue_scripts');
require 'theme.functions.php';



?>