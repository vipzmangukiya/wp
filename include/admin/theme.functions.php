<?php
add_action('admin_menu', 'add_theme_menu_item');
function add_theme_menu_item()
{
	add_menu_page("ARC Theme Setting", "ARC Theme", "manage_options", "arc-theme-options", "theme_settings_page", null, 99);


	
}

function theme_settings_page() {

    //require get_template_directory().'/include/admin/templates/arc-theme-tabs.php';



    ?>
     <style>
            input[type=text], input[type=search], input[type=radio], input[type=tel], input[type=time], input[type=url], input[type=week], input[type=password], input[type=checkbox], input[type=color], input[type=date], input[type=datetime], input[type=datetime-local], input[type=email], input[type=month], input[type=number], select, textarea{
                /* width: 50%; */
            }
        </style>
	    <div class="wrap">
	    <h1>
            <img src="http://staging.americanrubber.com/wp-content/themes/arc2017/images/logo.png" style="max-height: 40px; position: relative;top: 11px;" /> 
            ARC Theme's Setting
        </h1>
	  <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" id="data_save">
	        <div id="error_msg"></div>
            <div class="all_tabs">
                <ul>
                    <li><a href="#general_section" class="active">General</a></li>
                    <li><a href="#header_section">Header</a></li>
                    <li><a href="#slider_section">Slider</a></li>
                    <li><a href="#pages_section">Pages</a></li>
                    <li><a href="#sidebar_section">Sidebar</a></li>
                    <li><a href="#typography_section">Typography size and color</a></li>
                    <li><a href="#c_css_section">Custome CSS</a></li>
                    <li><a href="#c_script_section">Custome JavaScript</a></li>
                    <li><a href="#footer_section">Footer</a></li>
                </ul>
                 <div class='submit'><input  id='btn_general_save' class='button button-primary' value='Save Changes' type='submit' ></div> 
                <div class="tabs">
                    <?php require ('all_tabs.php'); ?>
                </div>
            </div> 

              
	  </form> 


		</div> 

    <?php
}



add_action( 'wp_ajax_custom_action_general_settings', 'custom_action_general_settings' );
add_action( 'wp_ajax_nopriv_custom_action_general_settings', 'custom_action_general_settings' );

add_action('wp_ajax_custom_action_general_settings_data','get_general_section');

function custom_action_general_settings() {
    $data = array();
    $keys = array_keys($_POST['data']);
    
    foreach($keys as $keyval) {
    
        $data[$keyval] = array();
    
        foreach($_POST['data'][$keyval] as $value) {  
    
            $data[$keyval] +=array($value['name']=>$value['value']); 
        }

    }

    for($i=0;$i<=count($keys); $i++) {
        general_data_update($data[$keys[$i]]);
    }
       

}
function get_general_section() {
    $data = array('blogname' => get_option('blogname') ,'blogdescription'=>get_option('blogdescription'),'admin_email'=>get_option('admin_email'),'logo_url'=>get_option('logo_url'));
    wp_send_json_success($data);
}

 function general_data_update($data) {
    foreach($data as $key => $value) {
        esc_attr(trim($key)) !="" ? update_option( $key, $data[$key]):'';
    }
    return true;
}


