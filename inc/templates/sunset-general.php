<h2>General Settings</h2>

<?php  settings_errors(); ?>

<form action="options.php" method="post" class="sunset-general-form">
	
	<?php  settings_fields('sunset-custom-general-options');?>
	<?php do_settings_sections('vipul_sunset_theme_general');?>
	<?php submit_button( 'Save Changes','primary','btnSubmit' );?>

</form>