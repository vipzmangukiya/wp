<?php 

function sunset_add_admin_page() {
//Create Admin  page
	add_menu_page( 'Sunset Theme Options', 'Sunset', 'manage_options', 'vipul_sunset', 'sunset_theme_create_page', get_template_directory_uri().'/img/sunset-icon.png', 110 );

//Submenu create on sunset admin page
	add_submenu_page( 'vipul_sunset', 'Sunset Sidebar Options ', 'Sidebar', 'manage_options', 'vipul_sunset', 'sunset_theme_create_page' );
	add_submenu_page( 'vipul_sunset', 'Sunset Theme Options', 'Theme Options', 'manage_options', 'vipul_sunset_theme','sunset_theme_support_page');
	add_submenu_page( 'vipul_sunset', 'Sunset Css Options', 'Custom CSS', 'manage_options', 'vipul_sunset_css','sunset_custom_css_page' );
	add_submenu_page( 'vipul_sunset', 'Sunset Contact Form', 'Contact Form','manage_options','vipul_sunset_theme_contact','vipul_sunset_contact_form_page');
	add_submenu_page( 'vipul_sunset', 'General Settings', 'General','manage_options','vipul_sunset_theme_general','vipul_sunset_theme_general_callback');

// Activate Custom Settings	
	add_action('admin_init','sunset_custom_settings');
	
}

add_action( 'admin_menu', 'sunset_add_admin_page' );
 
function sunset_custom_settings() {
	
	// Sidebar Options
		register_setting( 'sunset-settings-group','profile_picture');
		register_setting( 'sunset-settings-group', 'first_name');
		register_setting( 'sunset-settings-group','last_name');
		register_setting( 'sunset-settings-group','user_description');
		register_setting( 'sunset-settings-group','twitter_handler','sunset_sanitize_twitter_hanlder');
		register_setting( 'sunset-settings-group', 'facebook_hanlder');
		register_setting( 'sunset-settings-group', 'google_plus','sunset_sanitize_google_hanlder');

		add_settings_section( 'sunset-sidebar-options', 'Sidebar Options','sunset_sidebar_options', 'vipul_sunset');
		
		add_settings_field( 'sidebar-profile-picture', 'Profile Picture','sunset_sidebar_profile','vipul_sunset','sunset-sidebar-options');	
	    add_settings_field( 'sidebar-name', 'Full Name', 'sunset_sidebar_name', 'vipul_sunset','sunset-sidebar-options');
	 	add_settings_field( 'sidebar-user_desc', 'User Description', 'sunset_sidebar_user_desc', 'vipul_sunset','sunset-sidebar-options');
	 	add_settings_field( 'sidebar-twitter', 'Twitter Handler', 'sunset_sidebar_twitter', 'vipul_sunset', 'sunset-sidebar-options');
	 	add_settings_field( 'sidebar-facebook', 'Facebook', 'sunset_sidebar_facebook', 'vipul_sunset', 'sunset-sidebar-options');
	 	add_settings_field( 'sidebar-google-plus', 'Google', 'sunset_sidebar_googleplus', 'vipul_sunset','sunset-sidebar-options');

	 
	// Theme Supports Options
	 	register_setting('sunset-theme-support','post_formats');
	 	register_setting('sunset-theme-support','custom_header');
	 	register_setting('sunset-theme-support','custom_background');

	 	add_settings_section( 'sunset-theme-options', 'Theme Options', 'sunset_theme_options', 'vipul_sunset_theme');

	 	add_settings_field( 'post-formats', 'Post Formats', 'sunset_post_formats', 'vipul_sunset_theme','sunset-theme-options');
	 	add_settings_field( 'custom-header', 'Custom Header', 'sunset_custom_header', 'vipul_sunset_theme','sunset-theme-options');
	 	add_settings_field( 'custom-background', 'Custom Background', 'sunset_custom_background', 'vipul_sunset_theme','sunset-theme-options' );

	 // Contact form Options
	 	register_setting( 'sunset-contact-optins', 'activate_contact');

	 	add_settings_section( 'sunset-contact-section', 'Contact Form', 'sunset_contact_section','vipul_sunset_theme_contact');	

	 	add_settings_field( 'activate-form', 'Activate Contact Form', 'sunset_activate_contact', 'vipul_sunset_theme_contact', 'sunset-contact-section');

	 // Custom CSS Options

	 	register_setting( 'sunset-custom-css-options', 'sunset_css','sunset_sanitize_custom_css');

	 	add_settings_section( 'sunset-custom-css-section', 'Custom CSS', 'sunset_custom_css_section_callback','vipul_sunset_css' );

	 	add_settings_field( 'custom-css', 'Custom CSS Here', 'sunset_custom_css_callback', 'vipul_sunset_css', 'sunset-custom-css-section');

	// General Settings Options 	
	 	register_setting( 'sunset-custom-general-options', 'blogname');
	 	register_setting( 'sunset-custom-general-options', 'blogdescription');
	 	register_setting( 'sunset-custom-general-options', 'siteurl');
	 	register_setting( 'sunset-custom-general-options', 'home');
	 	register_setting( 'sunset-custom-general-options', 'admin_email');

	 	add_settings_section( 'sunset-custom-general-section', '', 'sunset_custom_general_section_callback','vipul_sunset_theme_general' );

	 	add_settings_field( 'general-title', '<label for="blogname">Site Title</label>', 'custom_general_site_title', 'vipul_sunset_theme_general', 'sunset-custom-general-section');

	 	add_settings_field( 'general-tagline', '<label for="blogdescription">Tagline</label>', 'custom_general_tagline', 'vipul_sunset_theme_general', 'sunset-custom-general-section');

	 	add_settings_field( 'general-wpURL', '<label for="siteurl">WordPress Address (URL)</label>', 'custom_general_wpURL', 'vipul_sunset_theme_general', 'sunset-custom-general-section');

	 	add_settings_field( 'general-siteURL', '<label for="home">Site Address (URL)</label>', 'custom_general_siteURL', 'vipul_sunset_theme_general', 'sunset-custom-general-section');

	 	add_settings_field( 'general-adminEmail', '<label for="admin_email">Email Address </label>', 'custom_general_email', 'vipul_sunset_theme_general', 'sunset-custom-general-section');
}

//General Settings  Function
	function sunset_custom_general_section_callback() {

	}
	function custom_general_site_title() {
		$blogname = esc_attr( get_option('blogname'));
		echo '<input type="text" id="blogname" name="blogname" class="regular-text" value="'.$blogname.'" placeholder="Site Title" />';
	}

	function custom_general_tagline() {
		$blogdescription = esc_attr( get_option('blogdescription'));
		echo '<input type="text" id="blogdescription" name="blogdescription" class="regular-text" value="'.$blogdescription.'" placeholder="Tag Line" /><p class="description" id="tagline-description">In a few words, explain what this site is about.</p>';
	}

	function custom_general_wpURL() {
		$siteurl = esc_attr( get_option('siteurl'));
		echo '<input type="text" id="siteurl" name="siteurl" class="regular-text" value="'.$siteurl.'" placeholder="WordPress URL" />';
	}

	function custom_general_siteURL() {
		$home = esc_attr( get_option('home'));
		echo '<input type="text" id="home" name="home" class="regular-text" value="'.$home.'" placeholder="Site URL" /><p class="description" id="home-description">Enter the address here if you <a href="https://codex.wordpress.org/Giving_WordPress_Its_Own_Directory">want your site home page to be different from your WordPress installation directory.</a></p>';
	}

	function custom_general_email() {
		$admin_email = esc_attr( get_option('admin_email'));
		echo '<input type="text" id="admin_email" name="admin_email" class="regular-text" value="'.$admin_email.'" placeholder="admin email" /><p class="description" id="admin-email-description">This address is used for admin purposes, like new user notification.</p>';
	}

//Custom css Function
	function sunset_custom_css_section_callback() {
		echo 'Customize Sunset Theme with your own CSS';
	}

	function sunset_custom_css_callback() {
		$css = get_option( 'sunset_css' );
		$checked = ( empty($css) ? '/* Sunset Theme Custom */' : $css);
		echo '<div id="customCss">'.$css.'</div><textarea name="sunset_css" id="sunset_css" style="display:none;visible:hidden">'.$css.'</textarea>';
	}

	 


// Contact Form Funcation

	function sunset_contact_section() {
		echo 'Activate or deactivate the Built-in Contact Form';
	}

	function sunset_activate_contact() {
		$options = get_option( 'activate_contact' );
		$checked = ( @$options == 1 ? 'checked' : '');
		echo '<label><input type="checkbox" id="activate_contact" name="activate_contact" value="1" '.$checked.' />Activate the Contact Form</label></br>';
	}
	

// Post formats Function 
	function sunset_theme_options() {
		echo 'Activate or deactivate Theme Support Options';
	}
	
	function sunset_post_formats() { // set post format type 
		$options =  get_option( 'post_formats');
		$formats = array('aside','gallery','link','quote','status','video','audio','chat');
		$output  = '';
		foreach ($formats as $value) {
			$checked = ( @$options[$value] == 1 ? 'checked' : '');
			$output .='<label><input type="checkbox" id="'.$value.'" name="post_formats['.$value.']" value="1" '.$checked.' />'.$value.'</label></br>';
		}
		echo $output;
	}

	function sunset_custom_header() {
		$options = get_option( 'custom_header' );
		$checked = ( @$options == 1 ? 'checked' : '');
		echo '<label><input type="checkbox" id="custom_header" name="custom_header" value="1" '.$checked.' />Activate the Custom Header</label></br>';
	}

	function sunset_custom_background() {
		$options = get_option( 'custom_background' );
		$checked = ( @$options == 1 ? 'checked' : '');
		echo '<label><input type="checkbox" id="custom_background" name="custom_background" value="1" '.$checked.' />Activate the Custom Background</label></br>';
	}
	

//Sidebar Options Function
	function sunset_sidebar_profile () {
		$profile_picture  = esc_attr( get_option( 'profile_picture' ));
		if( empty($profile_picture)) {
			echo '<input type="button" value="Upload Profile Picture" id="upload-button" class="button button-secondary"/><input type="hidden" name="profile_picture" id="profile-picture" value=""/> '; 
		} else {
			echo '<input type="button" value="Upload Profile Picture" id="upload-button" class="button button-secondary"/><input type="hidden" name="profile_picture" id="profile-picture" value="'.$profile_picture.'"/> <input type="button" class="button button-secondary" value="Remove" id="remove-picture">'; 
		}
	}

	function sunset_sidebar_name() {
		$firstName = esc_attr( get_option('first_name'));
		$lastName = esc_attr( get_option('last_name'));
		echo '<input type="text" name="first_name" value="'.$firstName.'" placeholder="First Name"/> <input type="text" name="last_name" value="'.$lastName.'" placeholder="Last Name"/>';
	}

	function sunset_sidebar_user_desc() {
		$user_dec = esc_attr( get_option('user_description'));
		echo '<input type="text" name="user_description" value="'.$user_dec.'" placeholder="User Description" /><p class="description">Write about user</p>';

	}

	function sunset_sidebar_googleplus() {
		$google = esc_attr( get_option( 'google_plus'));
		echo '<input type="text" name="google_plus" value ="'.$google.'" placeholder="Google Accout" /><p class="description"> Google username wihtout the @gmail.com character';
	}

	function sunset_sidebar_facebook() {
		$facebook = esc_attr( get_option( 'facebook_hanlder'));
		echo '<input type="text" name="facebook_hanlder" value="'.$facebook.'" placeholder="Facebook Accout" />';
	}

	function sunset_sidebar_twitter() {
		$twitter = esc_attr( get_option( 'twitter_handler'));
		echo '<input type="text" name="twitter_handler" value="'.$twitter.'" placeholder="Twitter Accout" / ><p class="description">Twitter username wihtout the @ character.';
	}

// Sanitization Settings

	function sunset_sanitize_twitter_hanlder( $input ) {
		$output = sanitize_text_field( $input );
		$output  = str_replace('@', '', $output);
		return $output;
	}

	function sunset_sanitize_google_hanlder( $input ) {
		$output = sanitize_text_field( $input );
		$output = str_replace('@gmail.com','',$output);
		return $output;
	}

	function sunset_sanitize_custom_css() {
		$output = esc_textarea( $input );
		return $output;
	}

// Template submenu functions
	function sunset_sidebar_options() {
		echo "Hello";
	}

	function sunset_theme_create_page() {
		
		require_once (get_template_directory() .'/inc/templates/sunset-admin.php');
	}

	function sunset_theme_support_page(){
		
		require_once (get_template_directory() .'/inc/templates/sunset-theme-supoort.php');
	}

	function sunset_custom_css_page() {
		
		require_once (get_template_directory() .'/inc/templates/sunset-custom-css.php');

	}

	function vipul_sunset_contact_form_page() {

		require_once ( get_template_directory() .'/inc/templates/sunset-contact-form.php');
	}

	function vipul_sunset_theme_general_callback() {
	
		require_once ( get_template_directory() .'/inc/templates/sunset-general.php');
	
	}