
jQuery(document).ready(function($) {
var uploadID = ''; /*setup the var*/
	jQuery('#upload_logo_button').click(function() {
	    uploadID = jQuery(this).prev('input'); /*grab the specific input*/
	    formfield = jQuery('#logo_url').attr('name');
	    tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
	    return false;
	});
	window.send_to_editor = function(html) {
	    imgurl = jQuery(html).filter("img").attr('src');
	    jQuery('#logo_url').val(imgurl); /*assign the value to the input*/
	    tb_remove();
	    console.log(html);
	};
});

jQuery(document).ready(function(){
	jQuery(".all_tabs ul li a").click(function(){
		jQuery(".all_tabs ul li a").removeClass('active');
		jQuery(this).addClass("active");
		var id = jQuery(this).attr('href');

		jQuery(".all_tabs .tabs .tab").removeClass('active');
		jQuery(id).addClass('active');	
		return false;
	});
});

jQuery(document).ready(function($) {
	
	jQuery('#data_save').on('submit', function(e) {
		e.preventDefault();
		var data = {};  
			$('form').each(function(index){  
			    data[this.id] = $(this).serializeArray();
			    //console.log(data);
			});
			 
			// console.log(data);
			jQuery.ajax({
				url:$(this).attr('action'),
				type: 'POST',
				data: {action: 'custom_action_general_settings',data:data},
			})
			.done(function(response) {
				$("#error_msg").html("<div id='setting-error-settings_updated' class='updated settings-error notice is-dismissible' style='border-bottom: 1px solid #26388b;border-top: 1px solid #26388b;border-right: 1px solid #26388b;border-left-color:#26388b'><p><strong>Settings saved.</strong></p></div>").fadeIn(400).delay(3000).fadeOut(400);;
			})
			.fail(function() {
				console.log("error");
			});
	});

	function general_tab() {
		$.ajax({
			url: 'http://localhost/wp/wp-admin/admin-ajax.php',
			type: 'POST',
			dataType: 'json',
			data: {action: 'custom_action_general_settings_data'}
		})
		.done(function(response) {
			console.log(response.data);
			$("#blogname").val(response.data.blogname);
			$("#blogdescription").val(response.data.blogdescription);
			$("#admin_email").val(response.data.admin_email);
			$("#logo_url").val(response.data.logo_url)
		})
		.fail(function() {
			console.log("error");
		});		
	}
	general_tab();
 
});