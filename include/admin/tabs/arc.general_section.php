<form  method="post" id="general_data">
	
<table class="form-table">
	<tbody>
		<tr>
			<th scope="row"><label for="blogname">Site Title</label></th>
			<td><input name="blogname" id="blogname" value="" class="regular-text" type="text"></td>
		</tr>

		<tr>
			<th scope="row"><label for="blogdescription">Tagline</label></th>
			<td><input name="blogdescription" id="blogdescription" aria-describedby="tagline-description" value="" class="regular-text" type="text">
			<p class="description" id="tagline-description">In a few words, explain what this site is about.</p></td>
		</tr>

		<tr>
			<th scope="row"><label for="upload_logo_button">Upload Logo</label></th>
			<td><input type="button" value="Upload Site Logo" id="upload_logo_button" class="button button-secondary">
			<input type="hidden" name="logo_url"  id="logo_url">
			</td>
		</tr>
		

	</tbody>
</table>

</form>
<!-- <p class="submit"><input  id="btn_general_save" class="button button-primary" value="Save Changes" type="button" ></p> -->
