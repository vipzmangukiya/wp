<?php
class arc_class{
	
	public $data;

	/* Arc General Classes
	 * @return general settings html
	 */	


	function arc_class_general(){
		require get_template_directory_uri(). '/include/admin/tabs/arc.general_section.php';
	}

	function arc_class_header() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.header_section.php';
	 	//return $this->data;
	}

	function arc_class_slider() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.slider_section.php';
	}

	function arc_class_pages() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.pages_section.php';
	}

	function arc_class_sidebar() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.sidebar_section.php';
	}

	function arc_class_typography() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.typography_section.php';
	}

	function arc_class_customcss() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.c_css_section.php';
	}

	function arc_class_customJavascript() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.c_script_section.php';
	}

	function arc_class_footer() {
	 	require get_template_directory_uri(). '/include/admin/tabs/arc.footer_section.php';
	}

}
?>