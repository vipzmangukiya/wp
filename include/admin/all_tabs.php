<?php
/* ARC deifne all setting page class and functions
 * pages have not any return value
 * Author Rahul Dungarani
 *
 */
	require ("tabs/class/arc.class.tabs.php");
	$obj_arc = new arc_class();
?>
	<!-- 
		* List all General settings
	-->
	<div id="general_section" class="tab active">
		<h2>General Settings</h2>
		<?php echo $obj_arc->arc_class_general(); ?>
	</div>
	<!-- End General settings -- >
	
	<!-- 
		* List all Header settings
	-->		
	<div id="header_section" class="tab">
		<h2>Header Settings</h2>
		<?php echo $obj_arc->arc_class_header(); ?>
	</div>
	<!-- End Header settings -- >
	
	<!-- 
		* List Slider Header settings
	-->
	<div id="slider_section" class="tab">
		<h2>Slider Settings</h2>
		<?php echo $obj_arc->arc_class_slider(); ?>
	</div>
	<!-- End Header settings -- >
	
	<!-- 
		* List all Pages settings
	-->
	<div id="pages_section" class="tab">
		<h2>Pages Settings</h2>
		<?php echo $obj_arc->arc_class_pages(); ?>
	</div>
	<!-- End Header settings -- >

	<!-- 
		* List Sider bar settings
	-->
	<div id="sidebar_section" class="tab">
		<h2>Sildebar Settings</h2>
		<?php echo $obj_arc->arc_class_sidebar(); ?>
	</div>
	<!-- End Sidebar settings -- >
	<!-- 
		* List all Pages settings
	-->
	<div id="typography_section" class="tab">
		<h2>Typography size and color Settings</h2>
		<?php echo $obj_arc->arc_class_typography(); ?>
	</div>
	<!-- End Header settings -- >
	<!-- 
		* List all Pages settings
	-->
	<div id="c_css_section" class="tab">
		<h2>Custome CSS Settings</h2>
		<?php echo $obj_arc->arc_class_customcss(); ?>
	</div>
	<!-- End Header settings -- >

	 <!-- 
		* List all Pages settings
	-->
	<div id="c_script_section" class="tab">
		<h2>Custome Javascript Settings</h2>
		<?php echo $obj_arc->arc_class_customJavascript(); ?>
	</div>
	<!-- End Header settings -- >

	<!-- 
		* List all Pages settings
	-->
	<div id="footer_section" class="tab">
		<h2>Footer  Settings</h2>
		<?php echo $obj_arc->arc_class_footer(); ?>
	</div>
	<!-- End Header settings -- >

 