<?php 

// Set Post formats 
$options = get_option( 'post_formats' );
$formats = array('aside','gallery','link','quote','status','video','audio','chat');
$output  = array();
foreach ($formats as $value) {
    $output[] = ( @$options[$value] == 1 ? $value : '');
}

if(!empty( $options ) ) {
	add_theme_support( 'post-formats', $output);
}

// set Custom Header Options 

$header = get_option( 'custom_header' );
if( @$header == 1) {
	add_theme_support ('custom-header');
}

// Set Custom Background Options
$background = get_option( 'custom_background' );
if( @$background == 1) {
	add_theme_support ('custom-background');
}
